extends AnimatedSprite

export var _angle: float = 0.0
export var _randomAngle: float = 0.0
export var _speed: float = 0.0
export var _randomSpeed: float = 0.0
export var _acceleration: float = 0.0
export var _rotation: float = 0.0
export var _randomRotation: float = 0.0
export var _fade: bool = true
export var _lifetime: float = -1.0
export var _randomLifetime: float = 0.0
export var _playbackSpeed: float = 1.0
export var _randomPlaybackSpeed: float = 0.0

var _remainingLifetime

func _ready() -> void:
	if _lifetime < 0.0:
		connect("animation_finished", self, "queue_free")
	else:
		_lifetime += rand_range(-_randomLifetime, _randomLifetime)

	_angle += rand_range(-_randomAngle, _randomAngle)
	_speed += rand_range(-_randomSpeed, _randomSpeed)
	_rotation += rand_range(-_randomRotation, _randomRotation)
	_playbackSpeed += rand_range(-_randomPlaybackSpeed, _randomPlaybackSpeed)
	_remainingLifetime = _lifetime
	speed_scale = _playbackSpeed
	play()


func _process(delta: float) -> void:
	if _lifetime > 0.0:
		_remainingLifetime -= delta
		if _remainingLifetime < 0.0:
			queue_free()
	_speed += _acceleration * delta
	global_position += _speed * delta * Vector2.RIGHT.rotated(deg2rad(_angle))
	global_rotation_degrees += _rotation * delta
	if _fade && _lifetime > 0.0:
		modulate.a = _remainingLifetime / _lifetime


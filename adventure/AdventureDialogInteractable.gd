extends AdventureInteractable

class_name AdventureDialogInteractable

export var _highlightPath: NodePath

onready var _highlight: Node = get_node(_highlightPath)

func _getDialogProps() -> Dictionary:
	return {}

func _getDialogPresetName() -> String:
	return ""

func _shouldHighlight() -> bool:
	return true

func _onInteract() -> void:
	AdventurePlayer.Instance().State = "interact"
	yield(DialogBox.Show(_getDialogProps(), _getDialogPresetName()), "completed")
	AdventurePlayer.Instance().State = "idle"

func _process(_delta: float) -> void:
	if _highlight:
		_highlight.visible = _shouldHighlight()

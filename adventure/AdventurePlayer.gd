extends KinematicBody2D

class_name AdventurePlayer

const kAnimationMapping: Dictionary = {
	"idle": "idle",
	"walk": "walk",
	"run": "run",
	"interact": "idle",
}

export var _moveSpeedWalk: float = 175.0
export var _moveSpeedRun: float = 400.0
export var _interactDistance: float = 20.0
export var _canRun: bool = true
export var _defaultZoneProperties: Resource
export var _footstepFramesWalk: Array = []
export var _footstepFramesRun: Array = []
export var _footstepOffset: Vector2 = Vector2.ZERO
onready var _animatedSprite: AnimatedSprite = $AnimatedSprite
var _fourWayOverlaps: FourWayOverlaps

onready var kSpriteOffset: Vector2 = $AnimatedSprite.offset

var Facing: int = Direction4.DOWN
var State: String = "idle"
var ZoneProperties: AdventureZoneProperties = null

static func Instance() -> AdventurePlayer:
	return GlobalReferences.Get("AdventurePlayer") as AdventurePlayer


func InteractableStates() -> Array:
	return ["idle", "walk", "run"]


func CanMoveStates() -> Array:
	return ["idle", "walk", "run"]


func CanInteract() -> bool:
	return InteractableStates().has(State)


func CanMove() -> bool:
	return CanMoveStates().has(State)


func _init() -> void:
	GlobalReferences.Set(self)


func _ready() -> void:
	_fourWayOverlaps = FourWayOverlaps.new()
	_fourWayOverlaps._distance = _interactDistance
	_fourWayOverlaps._collisionShape2D = "../CollisionShape2D"
	add_child(_fourWayOverlaps)
	_animatedSprite.connect("frame_changed", self, "_onSpriteFrameChanged")


func _footstep() -> void:
	var sfx: AudioStream = ZoneProperties.FootstepSfx()
	if sfx:
		AudioPlayer.PlaySound(ZoneProperties.FootstepSfx())
	var particle: PackedScene = ZoneProperties.FootstepParticle()
	if particle:
		var instance: Node2D = particle.instance()
		var offset: Vector2 = _footstepOffset
		if Facing == Direction4.LEFT:
			offset.x = -offset.x
		elif Facing == Direction4.UP || Facing == Direction4.DOWN:
			offset.x = 0
		instance.global_position = global_position + offset
		get_parent().add_child(instance)


func _onSpriteFrameChanged() -> void:
	if ZoneProperties == null:
		return

	if State == "walk" && _footstepFramesWalk.has(_animatedSprite.frame):
		_footstep()
	if State == "run" && _footstepFramesRun.has(_animatedSprite.frame):
		_footstep()


func _process(_delta: float) -> void:
	if CanInteract() && Input.is_action_just_pressed("ui_accept"):
		_tryInteract()


func _tryInteract() -> void:
	var overlaps: Array = _fourWayOverlaps.GetOverlaps(Facing)
	for overlap in overlaps:
		var interactable: AdventureInteractable = overlap as AdventureInteractable
		if interactable:
			interactable.Interact()


func _physics_process(delta: float) -> void:
	if CanMove():
		var moveDir: int = InputManager.Direction4FromInput()
		if moveDir != Direction4.NONE:
			Facing = moveDir
		var moveVector: Vector2 = Direction4.ToVector2(moveDir) * delta * _moveSpeed()
		var originalPos: Vector2 = global_position
		move_and_collide(moveVector)
		var moved: bool = global_position.distance_to(originalPos) > 0.1
		if moved:
			if _canRun && Input.is_action_pressed("adventure_run"):
				State = "run"
			else:
				State = "walk"
		else:
			State = "idle"

	var animState: String = kAnimationMapping[State]
	var animDirection: String = Direction4.ToString(Facing)
	if Facing == Direction4.LEFT:
		animDirection = Direction4.ToString(Direction4.RIGHT)
		_animatedSprite.flip_h = true
		_animatedSprite.offset = Vector2(-kSpriteOffset.x, kSpriteOffset.y)
	else:
		_animatedSprite.flip_h = false
		_animatedSprite.offset = kSpriteOffset
	if Facing == Direction4.UP || Facing == Direction4.DOWN:
		_animatedSprite.offset.x = 0
	var animation: String = "%s_%s" % [animState, animDirection]
	_animatedSprite.play(animation)

	ZoneProperties = _defaultZoneProperties
	var collisions: Array = PhysicsUtils.GetCollisions(self)
	for collision in collisions:
		if collision.collider is AdventureZone:
			ZoneProperties = collision.collider.Properties
			break


func _moveSpeed() -> float:
	var result: float = _moveSpeedWalk
	if _canRun && Input.is_action_pressed("adventure_run"):
		result = _moveSpeedRun
	return result

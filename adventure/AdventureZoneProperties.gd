extends Resource

class_name AdventureZoneProperties

export var _footstepSfx: Array
export var _footstepParticles: Array

func FootstepSfx() -> AudioStream:
	if _footstepSfx.size() <= 0:
		return null
	return _footstepSfx[randi() % _footstepSfx.size()]

func FootstepParticle() -> PackedScene:
	if _footstepParticles.size() <= 0:
		return null
	return _footstepParticles[randi() % _footstepParticles.size()]

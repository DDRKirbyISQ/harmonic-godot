extends AdventureInteractable


export var _targetScene: String
export var _targetWarp: String

export var _enterSound: AudioStream
export var _exitSound: AudioStream

export var _spawnDirection: String

export var _canEnter: bool = true

export var _fadeAudio: bool = false

export var _transitionDuration: float = 0.5
export var _transitionDelay: float = 0.0
export var _color: Color = Color.black

func _ready() -> void:
	if name == GlobalData.Get("adventurewarp_target", ""):
		_playerInside = true
		if _exitSound:
			AudioPlayer.PlaySound(_exitSound)
		AdventurePlayer.Instance().Facing = Direction4.FromString(_spawnDirection)
		AdventurePlayer.Instance().global_position = global_position


func _onEnter() -> void:
	if !_canEnter:
		return

	AdventurePlayer.Instance().State = "interact"
	GlobalData.Set("adventurewarp_target", _targetWarp)
	if _enterSound:
		AudioPlayer.PlaySound(_enterSound)
	if _fadeAudio:
		AudioPlayer.FadeAll(_transitionDuration)
	yield(ScreenTransitioner.TransitionOut(_transitionDuration, ScreenTransitioner.FADE, _color), "completed")
	yield(get_tree().create_timer(_transitionDelay), "timeout")

	# Need to change scene during physics update so collision detection works instantly
	yield(get_tree(), "physics_frame")
	if _fadeAudio:
		AudioPlayer.FadeAll(0.0, 1.0, false)
	get_tree().change_scene(_targetScene)
	ScreenTransitioner.TransitionIn(_transitionDuration, ScreenTransitioner.FADE, _color)
	if _fadeAudio:
		AudioPlayer.FadeAll(_transitionDuration, 1.0, false)

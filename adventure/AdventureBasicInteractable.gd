extends AdventureDialogInteractable

export var _dialogPreset: String

export var _text1: String
export var _text2: String
export var _text3: String
export var _repeatingText: String
var _texts: Array
var _repeat: bool = false


func _getDialogProps() -> Dictionary:
	var textIndex: int = min(InteractCount(), _texts.size() - 1)
	return {"text": _texts[textIndex]}

func _getDialogPresetName() -> String:
	return _dialogPreset

func _shouldHighlight() -> bool:
	return InteractCount() < _texts.size()

func _canInteract() -> bool:
	return _repeat || InteractCount() < _texts.size()

func _ready() -> void:
	if _text1 != null:
		_texts.push_back(_text1)
	if _text2 != null:
		_texts.push_back(_text2)
	if _text3 != null:
		_texts.push_back(_text3)
	if _repeatingText != null:
		_texts.push_back(_repeatingText)
		_repeat = true
	

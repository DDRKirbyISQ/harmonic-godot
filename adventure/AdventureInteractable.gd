extends CollisionObject2D

class_name AdventureInteractable

onready var kSaveDataKey: String = "adventureinteractable_" + get_script().get_path().get_file()

var _playerInside: bool = false

func Get(key: String, default):
	var dict: Dictionary = GlobalData.Get(kSaveDataKey, {})
	return dict.get(key, default)


func Set(key: String, value):
	var dict: Dictionary = GlobalData.Get(kSaveDataKey, {})
	dict[key] = value
	GlobalData.Set(kSaveDataKey, dict)


func InteractCount() -> int:
	return Get("interacted_count", 0)


func Interact() -> void:
	if !_canInteract():
		return

	_onInteract()
	Set("interacted_count", InteractCount() + 1)


func _canInteract() -> bool:
	return true

func _onInteract() -> void:
	pass


func _onInside() -> void:
	pass


func _onEnter() -> void:
	pass


func _onExit() -> void:
	pass


func _physics_process(_delta: float) -> void:
	var overlapsPlayer = PhysicsUtils.CollisionTest(self, GlobalReferences.Get("AdventurePlayer"))
	if !_playerInside && overlapsPlayer:
		_playerInside = true
		_onEnter()
	elif _playerInside && ! overlapsPlayer:
		_playerInside = false
		_onExit()

	if overlapsPlayer:
		_onInside()

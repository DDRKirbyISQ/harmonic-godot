# Usage:
# yield(ScreenTransitioner.TransitionOut(1, ScreenTransitioner.Type.DIAMONDS), "completed")
# # Change scenes here
# yield(ScreenTransitioner.TransitionIn(1, ScreenTransitioner.Type.DIAMONDS), "completed")
# # Transition done

extends CanvasLayer

enum {
	DIAMONDS = 0,
	FADE = 1,
}

const SHADERS: Dictionary = {
	DIAMONDS: preload("transition-diamonds.shader"),
	FADE: preload("transition-fade.shader"),
}

var _progress: float = 1.0
var _reverse: float = 1.0
onready var _colorRect: ColorRect = $ColorRect

func IsTransitioning() -> bool:
	return _progress < 1.0


func SetParams(params: Dictionary = {}) -> void:
	for param in params:
		_colorRect.material.set_shader_param(param, params[param])


func TransitionOut(duration: float, type: int, color: Color = Color.black, delay: float = 0.0) -> void:
	_colorRect.color = color
	_colorRect.material.shader = SHADERS[type]
	if _reverse > 0.5:
		_progress = 1.0 - _progress
	_reverse = 0
	_paramUpdate()
	$Tween.remove_all()
	$Tween.interpolate_method(
		self, "_onTweenUpdate", _progress, 1, duration, Tween.TRANS_LINEAR, Tween.EASE_IN, delay
	)
	$Tween.start()
	yield($Tween, "tween_completed")


func TransitionIn(duration: float, type: int, color: Color = Color.black, delay: float = 0.0) -> void:
	$ColorRect.color = color
	$ColorRect.material.shader = SHADERS[type]
	if _reverse < 0.5:
		_progress = 1.0 - _progress
	_reverse = 1.0
	_paramUpdate()
	$Tween.remove_all()
	$Tween.interpolate_method(
		self, "_onTweenUpdate", _progress, 1, duration, Tween.TRANS_LINEAR, Tween.EASE_IN, delay
	)
	$Tween.start()
	yield($Tween, "tween_completed")


func InstantOut(color: Color = Color.black) -> void:
	$ColorRect.color = color
	_reverse = 0.0
	_progress = 1.0
	_paramUpdate()


func InstantIn() -> void:
	_reverse = 1.0
	_progress = 1.0
	_paramUpdate()


func _ready() -> void:
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	_paramUpdate()


func _onTweenUpdate(progress: float) -> void:
	_progress = progress
	_paramUpdate()

func _paramUpdate() -> void:
	_colorRect.visible = _progress < 1.0 || _reverse < 0.5
	if _colorRect.visible:
		_colorRect.material.set_shader_param("reverse", _reverse)
		_colorRect.material.set_shader_param("progress", _progress)

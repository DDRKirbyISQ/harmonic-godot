shader_type canvas_item;

uniform float progress = 0.5;
uniform float reverse = 0.0;
uniform float diamondPixelSize = 10.0;
uniform vec2 uvOffset = vec2(1.0, 1.0);

bool transition(vec2 fragCoord, vec2 uv) {
	float testValue = progress * (1.0 + uvOffset.x + uvOffset.y) - dot(uv, uvOffset);
	testValue = testValue + reverse * (1.0 - 2.0 * testValue);
	bool nextImage = abs(fract(fragCoord.x / diamondPixelSize) - 0.5) + abs(fract(fragCoord.y / diamondPixelSize) - 0.5) > testValue;
	return nextImage;
}

void fragment() {
	if (transition(FRAGCOORD.xy, UV)) {
		discard;
	}
}

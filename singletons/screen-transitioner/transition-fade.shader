shader_type canvas_item;

uniform float progress = 0.5;
uniform float reverse = 1.0;

void fragment() {
	COLOR.a = COLOR.a * (progress + reverse * (1.0 - 2.0 * progress));
}

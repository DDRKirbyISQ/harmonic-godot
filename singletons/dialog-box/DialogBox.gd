# Usage:
# yield(
# 	DialogBox.Show(
# 		{
# 			"text": "Some text here, rich text supported.",
# 			"color": Color(0.5, 1.0, 1.0),
# 			"font": _myFont,
# 			"charsPerSecond": 5,
# 			"fastTextMultiplier": 3,
# 			"soundLoop": _mySound,
# 			"image": _myImage,
# 			"closeAfterSeconds": 3.0,
# 			"allowSpeed": false,
# 			"showArrow": true,
# 			"choices": ["Yes", "Maybe", "No"],
# 			"choiceColor": Color(0.5, 0.5, 0.5),
# 			"choiceSelectedColor1": Color(1.0, 1.0, 0.5),
# 			"choiceSelectedColor2": Color(1.0, 0.5, 0.5),
# 			"pause": true,
# 			"top": true,
# 		},
# 		_presetName
# 	),
# 	"completed"
# )
# print(DialogBox.SelectedChoice)

extends CanvasLayer

var SelectedChoice: int = 0

export var _rootPath: NodePath
var _root: VBoxContainer

export var _richTextLabelPath: NodePath
var _richTextLabel: RichTextLabel

export var _textureRectPath: NodePath
var _textureRect: TextureRect

export var _choiceParentPath: NodePath
var _choiceLabels: Array

export var _arrowPath: NodePath
var _arrow: Control

var _audioStreamPlayer: AudioStreamPlayer

export var _defaultCharsPerSecond: float = 20.0
export var _defaultFastTextMultiplier: float = 3.0
export var _defaultAudioInterval: float = 1.0
export var _defaultSoundLoop: AudioStream
export var _defaultChoiceColor: Color = Color(0.5, 0.5, 0.5)
export var _defaultChoiceSelectedColor1: Color = Color.yellow
export var _defaultChoiceSelectedColor2: Color = Color.white
export var _soundVolume: float = 1.0

export var _choiceSelectSound: AudioStream
export var _choiceConfirmSound: AudioStream
var _defaultFont: Font

var _presets: Dictionary = {}


func _ready() -> void:
	pause_mode = Node.PAUSE_MODE_PROCESS
	_root = get_node(_rootPath)
	_richTextLabel = get_node(_richTextLabelPath)
	_defaultFont = _richTextLabel.get_font("normal_font")
	_textureRect = get_node_or_null(_textureRectPath)
	_arrow = get_node_or_null(_arrowPath)
	_root.visible = false
	_audioStreamPlayer = AudioStreamPlayer.new()
	var choiceParent: Node = get_node_or_null(_choiceParentPath)
	_choiceLabels = choiceParent.get_children() if choiceParent else []
	add_child(_audioStreamPlayer)
	_audioStreamPlayer.set_volume_db(linear2db(_soundVolume))


func LoadPresets(presets: Dictionary) -> void:
	_presets = presets


func Show(newProps: Dictionary, presetName: String = "") -> void:
	SelectedChoice = 0

	if _arrow:
		_arrow.visible = false
	for choiceLabel in _choiceLabels:
		choiceLabel.visible = false
	_root.visible = true

	var preset: Dictionary = _presets.get(presetName, {})
	var props = DictionaryUtils.Merge(preset, newProps)
	var charsPerSecond: float = props.get("charsPerSecond", _defaultCharsPerSecond)
	var fastTextMultiplier: float = props.get("fastTextMultiplier", _defaultFastTextMultiplier)
	var soundLoop: AudioStream = props.get("soundLoop", _defaultSoundLoop)
	var allowSpeed: bool = props.get("allowSpeed", true)
	var color: Color = props.get("color", Color.white)
	var font: Font = props.get("font", _defaultFont)
	var choiceColor: Color = props.get("choiceColor", _defaultChoiceColor)
	var choiceSelectedColor1: Color = props.get(
		"choiceSelectedColor1", _defaultChoiceSelectedColor1
	)
	var choiceSelectedColor2: Color = props.get(
		"choiceSelectedColor2", _defaultChoiceSelectedColor2
	)
	var choices: Array = props.get("choices", [])
	var pause: bool = props.get("pause", false)
	var text: String = props.get("text", "")
	var top: bool = props.get("top", false)

	_root.alignment = BoxContainer.ALIGN_BEGIN if top else BoxContainer.ALIGN_END

	if pause:
		get_tree().paused = true

	if _textureRect:
		if props.has("image"):
			_textureRect.texture = props.get("image")
			_textureRect.visible = true
		else:
			_textureRect.visible = false

	_richTextLabel.add_font_override("normal_font", font)
	_richTextLabel.bbcode_text = text
	_richTextLabel.visible_characters = 0
	_richTextLabel.modulate = color
	for i in range(_choiceLabels.size()):
		_choiceLabels[i].visible = false
		if i < choices.size():
			_choiceLabels[i].add_font_override("normal_font", font)
			_choiceLabels[i].bbcode_text = choices[i]
			_choiceLabels[i].modulate = choiceColor

	if soundLoop:
		AudioUtils.SetLoop(soundLoop, true)
		_audioStreamPlayer.stream = soundLoop
		_audioStreamPlayer.play()

	if text:
		yield(_showText(charsPerSecond, fastTextMultiplier, allowSpeed), "completed")

	if soundLoop:
		AudioUtils.SetLoop(soundLoop, false)

	if props.get("showArrow", false) && _arrow:
		_arrow.visible = true

	for i in range(choices.size()):
		_choiceLabels[i].visible = true

	if props.has("closeAfterSeconds"):
		yield(get_tree().create_timer(props.get("closeAfterSeconds")), "timeout")
	else:
		yield(
			_waitForInput(choices, choiceColor, choiceSelectedColor1, choiceSelectedColor2),
			"completed"
		)

	_root.visible = false

	if pause:
		get_tree().paused = false


func _showText(charsPerSecond: float, fastTextMultiplier: float, allowSpeed: bool) -> void:
	var text: String = _richTextLabel.text
	var totalTime: float = 0
	var colon:int = text.find(':')
	if colon != -1:
		totalTime = (colon + 1) / charsPerSecond

	while true:
		var beforeTime: float = OS.get_ticks_msec()
		yield(get_tree(), "idle_frame")
		if Input.is_action_just_pressed("dialog_box_speed"):
			allowSpeed = true
		var afterTime: float = OS.get_ticks_msec()
		var increment: float = (afterTime - beforeTime) * 0.001
		if allowSpeed && Input.is_action_pressed("dialog_box_speed"):
			increment *= fastTextMultiplier
		totalTime += increment
		var visibleChars: int = floor(totalTime * charsPerSecond) as int
		if visibleChars != _richTextLabel.visible_characters:
			_richTextLabel.visible_characters = visibleChars

		if _richTextLabel.visible_characters >= text.length():
			break


func _waitForInput(choices: Array, choiceColor: Color, selectedColor1: Color, selectedColor2: Color) -> void:
	if choices.size() > 0:
		for i in range(_choiceLabels.size()):
			_choiceLabels[i].modulate = choiceColor
		yield(get_tree().create_timer(0.5), "timeout")

	while true:
		if choices.size() > 0:
			var lerpFactor: float = sin(OS.get_ticks_msec() * .001 * 2 * PI / 1) * 0.5 + 0.5
			var selectedColor: Color = lerp(selectedColor1, selectedColor2, lerpFactor)
			for i in range(_choiceLabels.size()):
				_choiceLabels[i].modulate = selectedColor if i == SelectedChoice else choiceColor
			if Input.is_action_just_pressed("ui_down"):
				if _choiceSelectSound:
					AudioPlayer.PlaySound(_choiceSelectSound)
				SelectedChoice += 1
			if Input.is_action_just_pressed("ui_up"):
				if _choiceSelectSound:
					AudioPlayer.PlaySound(_choiceSelectSound)
				SelectedChoice -= 1
			SelectedChoice = (SelectedChoice + choices.size()) % choices.size()

		yield(get_tree(), "idle_frame")

		if Input.is_action_just_pressed("ui_accept"):
			if choices.size() > 0 && _choiceConfirmSound:
				AudioPlayer.PlaySound(_choiceConfirmSound)
			yield(get_tree(), "idle_frame")
			return

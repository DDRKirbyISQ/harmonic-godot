# Usage:
#
# const music = preload("res://music/music.ogg")
# AudioPlayer.PlayMusic(music)
# yield(AudioPlayer.FadeMusic(music, 3), "completed")
#
# const sfx = preload("res://sfx/sfx.wav")
# yield(AudioPlayer.playSound(sfx, 1), "completed")

extends Node

const SFX_STREAMS: int = 32

var _musicPlayer: AudioStreamPlayer = AudioStreamPlayer.new()
var _musicTween: Tween = Tween.new()
var _fadeTween: Tween = Tween.new()
var _sfxPlayers: Array = []
var _playedSounds: Dictionary
var _masterBusIndex: int = AudioServer.get_bus_index("Master")
var _musicPlaying: bool = false


func PlayMusic(stream: AudioStream, replay: bool = false, bus: String = "Music") -> void:
	_musicTween.remove_all()
	_musicPlayer.bus = bus
	_musicPlayer.volume_db = 0
	_musicPlayer.stream_paused = false
	if _musicPlayer.stream != stream:
		_musicPlayer.stream = stream
		_musicPlayer.play()
		_musicPlaying = true
	elif replay || !_musicPlaying:
		_musicPlayer.play()
		_musicPlaying = true


func StopMusic() -> void:
	_musicPlayer.stop()
	_musicPlaying = false


func FadeMusic(duration: float, targetVolume: float = 0, stopAtEnd: bool = true) -> void:
	_musicTween.remove_all()

	if duration == 0.0:
		_onMusicTweenUpdate(targetVolume)
		if stopAtEnd:
			StopMusic()
		return

	_musicTween.interpolate_method(
		self,
		"_onMusicTweenUpdate",
		db2linear(_musicPlayer.volume_db),
		targetVolume,
		duration,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	_musicTween.start()
	yield(_musicTween, "tween_completed")
	if stopAtEnd:
		StopMusic()

func FadeAll(duration: float, targetVolume: float = 0, stopAtEnd: bool = true) -> void:
	_fadeTween.remove_all()

	if duration == 0.0:
		_onFadeTweenUpdate(targetVolume)
		if stopAtEnd:
			StopMusic()
		return

	_fadeTween.interpolate_method(
		self,
		"_onFadeTweenUpdate",
		db2linear(AudioServer.get_bus_volume_db(_masterBusIndex)),
		targetVolume,
		duration,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	_fadeTween.start()
	yield(_fadeTween, "tween_completed")
	if stopAtEnd:
		StopMusic()


func PlaySound(stream: AudioStream, volume: float = 1, pitch_scale: float = 1, bus: String = "SFX") -> void:
	if _playedSounds.has(stream):
		return
	_playedSounds[stream] = true
	for sfxPlayer in _sfxPlayers:
		if !sfxPlayer.playing:
			sfxPlayer.stream = stream
			sfxPlayer.bus = bus
			sfxPlayer.volume_db = linear2db(volume)
			sfxPlayer.pitch_scale = pitch_scale
			sfxPlayer.play()
			yield(sfxPlayer, "finished")
			return


func _ready() -> void:
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	_musicPlayer.name = "MusicPlayer"
	_musicTween.name = "MusicTween"
	add_child(_musicPlayer)
	add_child(_musicTween)
	add_child(_fadeTween)
	for i in range(SFX_STREAMS):
		_sfxPlayers.append(AudioStreamPlayer.new())
		_sfxPlayers[i].name = "SfxPlayer %s" % i
		add_child(_sfxPlayers[i])


func _process(_delta: float) -> void:
	_playedSounds.clear()


func _onMusicTweenUpdate(volume: float) -> void:
	_musicPlayer.volume_db = linear2db(volume)

func _onFadeTweenUpdate(volume: float) -> void:
	AudioServer.set_bus_volume_db(_masterBusIndex, linear2db(volume))

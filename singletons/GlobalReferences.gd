# For temporary global references (singletons that aren't auto-load).

extends Node

var _dictionary: Dictionary = {}


func Set(node: Node) -> void:
	var key: String = node.get_script().get_path().get_file()
	key = key.substr(0, key.length() - 3)
	_dictionary[key] = node


func Get(scriptName: String) -> Node:
	return _dictionary.get(scriptName, null)

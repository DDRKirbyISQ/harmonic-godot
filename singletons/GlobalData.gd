extends Node

var _data: Dictionary = {}


func Get(key: String, default):
	return _data.get(key, default)


func Set(key: String, value):
	_data[key] = value

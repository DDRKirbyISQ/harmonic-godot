extends Node


var _index: int = 0

func _input(event: InputEvent):
	if !OS.is_debug_build():
		return

	if event is InputEventKey:
		# Printscreen on release
		if !event.is_pressed() && !event.is_echo() && (event.scancode == KEY_PRINT || event.scancode == 16777256):
			Screenshot()

func Screenshot() -> void:
	_createPath()
	var path: String = _nextPath()
	var image: Image = get_viewport().get_texture().get_data()
	image.flip_y()
	image.save_png(path)

func _createPath() -> void:
	var dir: Directory = Directory.new()
	dir.open(ProjectSettings.globalize_path("res://"))
	dir.make_dir("../screenshots")

func _nextPath() -> String:
	while true:
		var path: String = ProjectSettings.globalize_path("res://") + "/../screenshots/screenshot_%03d.png" % _index
		_index += 1
		var file: File = File.new()
		if !file.file_exists(path):
			return path
	return ""

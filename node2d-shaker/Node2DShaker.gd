class_name Node2DShaker

static func Shake(target: Node2D, duration: float, amount: float, resetZero: bool = true) -> void:
	var originalPosition: Vector2 = target.position
	if resetZero:
		originalPosition = Vector2.ZERO
	var startTimeMs: int = OS.get_ticks_msec()
	var endTimeMs: int = int(round(startTimeMs + duration * 1000))
	while OS.get_ticks_msec() < endTimeMs:
		if ! is_instance_valid(target):
			return
		if target.get_tree().paused:
			return
		target.position = originalPosition + Vector2Utils.Random(amount)
		yield(target.get_tree(), "idle_frame")
	if ! is_instance_valid(target):
		return
	target.position = originalPosition

static func ShakeCameraOffset(target: Camera2D, duration: float, amount: float, resetZero: bool = true) -> void:
	var originalPosition: Vector2 = target.offset
	if resetZero:
		originalPosition = Vector2.ZERO
	var startTimeMs: int = OS.get_ticks_msec()
	var endTimeMs: int = int(round(startTimeMs + duration * 1000))
	while OS.get_ticks_msec() < endTimeMs:
		if ! is_instance_valid(target):
			return
		if target.get_tree().paused:
			return
		target.offset = originalPosition + Vector2Utils.Random(amount)
		yield(target.get_tree(), "idle_frame")
	if ! is_instance_valid(target):
		return
	target.offset = originalPosition
	
extends Node2D

class_name FourWayOverlaps

export var _collisionShape2D: NodePath

export var _distance: float = 20.0

var _leftArea: Area2D = Area2D.new()
var _downArea: Area2D = Area2D.new()
var _upArea: Area2D = Area2D.new()
var _rightArea: Area2D = Area2D.new()
var _areas: Array = [_leftArea, _downArea, _upArea, _rightArea]


func _ready() -> void:
	var collider: CollisionShape2D = get_node(_collisionShape2D) as CollisionShape2D
	var rectangleShape: RectangleShape2D = collider.shape as RectangleShape2D
	global_position = collider.global_position
	for area in _areas:
		add_child(area)

	var rectX = RectangleShape2D.new()
	rectX.extents = Vector2(_distance * 0.5, rectangleShape.extents.y)
	var rectY = RectangleShape2D.new()
	rectY.extents = Vector2(rectangleShape.extents.x, _distance * 0.5)

	var collisionShapeLeft: CollisionShape2D = CollisionShape2D.new()
	collisionShapeLeft.shape = rectX
	var collisionShapeDown: CollisionShape2D = CollisionShape2D.new()
	collisionShapeDown.shape = rectY
	var collisionShapeUp: CollisionShape2D = CollisionShape2D.new()
	collisionShapeUp.shape = rectY
	var collisionShapeRight: CollisionShape2D = CollisionShape2D.new()
	collisionShapeRight.shape = rectX

	_leftArea.add_child(collisionShapeLeft)
	_downArea.add_child(collisionShapeDown)
	_upArea.add_child(collisionShapeUp)
	_rightArea.add_child(collisionShapeRight)

	_leftArea.position = Vector2(-_distance * 0.5 - rectangleShape.extents.x, 0)
	_downArea.position = Vector2(0, _distance * 0.5 + rectangleShape.extents.y)
	_upArea.position = Vector2(0, -_distance * 0.5 - rectangleShape.extents.y)
	_rightArea.position = Vector2(_distance * 0.5 + rectangleShape.extents.x, 0)


func GetOverlaps(facing: int) -> Array:
	var collisions: Array = PhysicsUtils.GetCollisions(_areas[facing])
	var result: Array = []
	for collision in collisions:
		result.push_back(collision.collider)
	for area in _areas:
		result.erase(area)
	result.sort_custom(self, "_distanceSort")
	return result


func _distanceSort(a: Node2D, b: Node2D) -> bool:
	return (
		a.global_position.distance_squared_to(global_position)
		< b.global_position.distance_squared_to(global_position)
	)

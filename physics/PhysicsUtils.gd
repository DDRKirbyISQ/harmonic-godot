class_name PhysicsUtils

static func CollisionTest(object1: CollisionObject2D, object2: CollisionObject2D) -> bool:
	var space_state = object1.get_world_2d().direct_space_state
	var shapeQuery:Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	shapeQuery.collide_with_areas = true
	for owner in object1.get_shape_owners():
		shapeQuery.transform = object1.shape_owner_get_owner(owner).global_transform
		for i in object1.shape_owner_get_shape_count(owner):
			shapeQuery.set_shape(object1.shape_owner_get_shape(owner, i))
			for collision in space_state.intersect_shape(shapeQuery):
				if collision.collider == object2:
					return true
	return false

static func GetCollisions(object1: CollisionObject2D) -> Array:
	var space_state = object1.get_world_2d().direct_space_state
	var shapeQuery:Physics2DShapeQueryParameters = Physics2DShapeQueryParameters.new()
	shapeQuery.collide_with_areas = true
	shapeQuery.exclude = [object1]
	var result:Array = []
	for owner in object1.get_shape_owners():
		shapeQuery.transform = object1.shape_owner_get_owner(owner).global_transform
		for i in object1.shape_owner_get_shape_count(owner):
			shapeQuery.set_shape(object1.shape_owner_get_shape(owner, i))
			result.append_array(space_state.intersect_shape(shapeQuery))
	return result

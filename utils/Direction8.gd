class_name Direction8

enum {
	NONE = -1,
	EAST = 0,
	SOUTHEAST = 1,
	SOUTH = 2,
	SOUTHWEST = 3,
	WEST = 4,
	NORTHWEST = 5,
	NORTH = 6,
	NORTHEAST = 7,
}

static func ToVector2(direction: int) -> Vector2:
	match direction:
		EAST:
			return Vector2(1, 0)
		SOUTHEAST:
			return Vector2(1, 1)
		SOUTH:
			return Vector2(0, 1)
		SOUTHWEST:
			return Vector2(-1, 1)
		WEST:
			return Vector2(-1, 0)
		NORTHWEST:
			return Vector2(-1, -1)
		NORTH:
			return Vector2(0, -1)
		NORTHEAST:
			return Vector2(1, -1)
	return Vector2.ZERO

static func Opposite(direction: int) -> int:
	match direction:
		EAST:
			return WEST
		SOUTHEAST:
			return NORTHWEST
		SOUTH:
			return NORTH
		SOUTHWEST:
			return NORTHEAST
		WEST:
			return EAST
		NORTHWEST:
			return SOUTHEAST
		NORTH:
			return SOUTH
		NORTHEAST:
			return SOUTHWEST
	return NONE

static func ToString(direction: int) -> String:
	match direction:
		EAST:
			return "east"
		SOUTHEAST:
			return "southeast"
		SOUTH:
			return "south"
		SOUTHWEST:
			return "southwest"
		WEST:
			return "west"
		NORTHWEST:
			return "northwest"
		NORTH:
			return "north"
		NORTHEAST:
			return "northeast"
	return "none"

static func FromString(direction: String) -> int:
	match direction.to_lower():
		"east":
			return EAST
		"southeast":
			return SOUTHEAST
		"south":
			return SOUTH
		"southwest":
			return SOUTHWEST
		"west":
			return WEST
		"northwest":
			return WEST
		"north":
			return NORTH
		"northeast":
			return NORTHEAST
	return NONE

static func Random() -> int:
	return randi() % 8

static func FromVector2(vector: Vector2) -> int:
	if vector.angle() > -7 * PI / 8 && vector.angle() <= -5 * PI / 8:
		return NORTHWEST
	if vector.angle() > -5 * PI / 8 && vector.angle() <= -3 * PI / 8:
		return NORTH
	if vector.angle() > -3 * PI / 8 && vector.angle() <= -PI / 8:
		return NORTHEAST
	if vector.angle() > -PI / 8 && vector.angle() <= PI / 8:
		return EAST
	if vector.angle() > PI / 8 && vector.angle() <= 3 * PI / 8:
		return SOUTHEAST
	if vector.angle() > 3 * PI / 8 && vector.angle() <= 5 * PI / 8:
		return SOUTH
	if vector.angle() > 5 * PI / 8 && vector.angle() <= 7 * PI / 8:
		return SOUTHWEST
	return WEST

class_name Direction4

enum { NONE = -1, LEFT = 0, DOWN = 1, UP = 2, RIGHT = 3 }

static func ToVector2(direction: int) -> Vector2:
	match direction:
		LEFT:
			return Vector2.LEFT
		DOWN:
			return Vector2.DOWN
		UP:
			return Vector2.UP
		RIGHT:
			return Vector2.RIGHT
	return Vector2.ZERO

static func Opposite(direction: int) -> int:
	match direction:
		LEFT:
			return RIGHT
		DOWN:
			return UP
		UP:
			return DOWN
		RIGHT:
			return LEFT
	return NONE

static func RotateCW(direction: int) -> int:
	match direction:
		LEFT:
			return UP
		DOWN:
			return LEFT
		UP:
			return RIGHT
		RIGHT:
			return DOWN
	return NONE

static func RotateCCW(direction: int) -> int:
	match direction:
		LEFT:
			return DOWN
		DOWN:
			return RIGHT
		UP:
			return LEFT
		RIGHT:
			return UP
	return NONE

static func IsHorizontal(direction: int) -> bool:
	return direction == LEFT || direction == RIGHT

static func IsVertical(direction: int) -> bool:
	return direction == UP || direction == DOWN

static func ToString(direction: int) -> String:
	match direction:
		LEFT:
			return "left"
		DOWN:
			return "down"
		UP:
			return "up"
		RIGHT:
			return "right"
	return "none"

static func FromString(direction: String) -> int:
	match direction.to_lower():
		"left":
			return LEFT
		"down":
			return DOWN
		"up":
			return UP
		"right":
			return RIGHT
	return NONE

static func Random() -> int:
	return randi() % 4

static func FromVector2(vector:Vector2) -> int:
	if vector.angle() > -3 * PI / 4 && vector.angle() <= -PI / 4:
		return UP
	if vector.angle() > -PI / 4 && vector.angle() <= PI / 4:
		return RIGHT
	if vector.angle() > PI / 4 && vector.angle() <= 3 * PI / 4:
		return DOWN
	return LEFT

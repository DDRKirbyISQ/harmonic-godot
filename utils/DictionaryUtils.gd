class_name DictionaryUtils

static func Merge(source: Dictionary, overwriter: Dictionary) -> Dictionary:
	var result: Dictionary = source.duplicate()
	for key in overwriter:
		result[key] = overwriter[key]
	return result

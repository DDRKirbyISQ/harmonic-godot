class_name Vector2Utils

static func Random(length: float = 1.0) -> Vector2:
	var result: Vector2 = Vector2.RIGHT * length
	return result.rotated(rand_range(0, 2 * PI))

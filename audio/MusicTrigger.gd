extends Node

export var music: AudioStream


func _ready() -> void:
	AudioPlayer.PlayMusic(music)
